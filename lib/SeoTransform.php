<?php

namespace Mangahigh\TwigExtensions;

use \Twig_Extension;
use \Twig_SimpleFilter;

class SeoTransform extends Twig_Extension
{
    public function __construct($callable = null)
    {
        $this->callable = $callable;
    }
    public function getFilters()
    {
        return array(
            new Twig_SimpleFilter('seoTransform', function($string, $spaceChar = '-') {
                if($this->callable) {
                    $callable = $this->callable;
                    return $callable($string, $spaceChar);
                }
                return $this->seoTransform($string, $spaceChar);
            }),
        );
    }

    public function getName()
    {
        return 'SeoTransform';
    }

    private function seoTransform($string, $spaceChar)
    {
        $string = str_replace(" ", $spaceChar, mb_strtolower($string));
        $string = str_replace($spaceChar.$spaceChar, $spaceChar, mb_strtolower($string));

        return $string;
    }
}
