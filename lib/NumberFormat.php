<?php

namespace Mangahigh\TwigExtensions;

class NumberFormat extends \Twig_Extension
{

    public function getFilters()
    {
        return array(
             new \Twig_SimpleFilter('nformat', function($float, $decimals=null) {
                return $this->nformat($float, $decimals);
             }),
        );
    }

    public function getName()
    {
        return 'NumberFormat';
    }

    private function nformat($float, $decimals)
    {
        if(!$decimals && $float != (int) $float) {
            $decimals = 2;
        }

        $locale = localeconv();
        return number_format($float, $decimals, $locale['decimal_point'], $locale['mon_thousands_sep']);
    }
}
