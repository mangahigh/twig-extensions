<?php

namespace Mangahigh\TwigExtensions;

class Paragraph extends \Twig_Extension
{

    public function getFilters()
    {
        return array(
             new \Twig_SimpleFilter('nl2p', function($msg) {
                return $this->translate($msg);
             }, array('is_safe' => array('html')))
        );
    }

    public function getName()
    {
        return 'nl2p';
    }

    private function translate($msg)
    {
        if(strpos($msg, "\n\n")) {
            // convert them to paragraphs
            $msg = '<p>'.str_replace("\n\n", '</p><p>', trim($msg)).'</p>';
        }

        return nl2br($msg);
    }
}
