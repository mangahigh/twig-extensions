<?php

namespace Mangahigh\TwigExtensions;

use \Twig_Extension;
use \Twig_SimpleFilter;

class Appurl extends Twig_Extension
{
    private $locale;
    private $appRoot;

    public function __construct($locale, $appRoot)
    {
        $this->appRoot = $appRoot;
        $this->locale = strtolower(str_replace('_', '-', $locale));
    }

    public function getFilters()
    {
        return array(
             new Twig_SimpleFilter('appurl', function($path) {
                return $this->appurl($path);
             }),
        );
    }

    public function getName()
    {
        return 'Appurl';
    }

    private function appurl($path)
    {
        $url = preg_replace('/\/+/', '/', '/'.$this->locale.'/'.$path);
        return $this->appRoot.$url;
    }
}
