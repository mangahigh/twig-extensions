<?php

namespace Mangahigh\TwigExtensions;

use \Twig_Extension;
use \Twig_SimpleFilter;

class CacheBuster extends Twig_Extension
{
    private $tokens;

    public function __construct($cachebusterFile)
    {
        if (file_exists($cachebusterFile)) {
            $this->tokens = include $cachebusterFile;
        }
    }

    public function getFilters()
    {
        return array(
             new Twig_SimpleFilter('cachebust', function($path) {
                return $this->cachebust($path);
             }),
        );
    }

    public function getName()
    {
        return 'CacheBuster';
    }

    private function cachebust($path)
    {
        if (is_null($this->tokens)) {
            return $path;
        }

        // Strip leading slash for look-up
        $key = ltrim($path, '/');

        // Do this now before we alter key
        $pieces = explode('/', $key, 2);
        if (count($pieces) != 2) {
            // no idea where to add the token...
            return $path;
        }

        // Search for each successive parent directory in the tokens
        while (!array_key_exists($key, $this->tokens)) {
            $pos = strrpos($key, '/');
            if ($pos === FALSE) {
                // Exhausted our search space
                return $path;
            } else {
                $key = substr($key, 0, $pos);
            }
        }

        // Ternary tries to preserve leading slash...
        return (($path[0] == '/') ? '/' : '').$pieces[0].'/'.$this->tokens[$key].'/'.$pieces[1];
    }
}
