<?php

namespace Mangahigh\TwigExtensions;

use \Twig_Extension;
use \Twig_SimpleFilter;

class Url extends Twig_Extension
{
    private $locale;

    public function __construct($locale)
    {
        $this->locale = strtolower(str_replace('_', '-', $locale));
    }

    public function getFilters()
    {
        return array(
             new Twig_SimpleFilter('url', function($path) {
                return $this->url($path);
             }),
        );
    }

    public function getName()
    {
        return 'Url';
    }

    private function url($path)
    {
        $url = preg_replace('/\/+/', '/', '/'.$this->locale.'/'.$path);
        return $url;
    }
}
